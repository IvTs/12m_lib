#ifndef MATHEMATICS_H_
#define MATHEMATICS_H_

#define MASS_RES_SIZE 4 //������ ������� ������������� -1
#define MASS_PRES_SIZE 6 //������ ������� �������� -1

#define u1 mass_value[line][col + 1]
#define u2 mass_value[line][col]
#define p1 mass_pres[col + 1]
#define p2 mass_pres[col]

double mass_value[4][6] = {
{	-2.9095,	6.9155,		21.615,		45.953,		70.345,		94.545	},
{	-2.4175,	7.4425,		22.209,		46.726,		71.1305,	95.428	},
{	-1.957,		7.921,		22.713,		47.2845,	71.749,		96.117	},
{	-1.194,		8.6935,		23.5085,	48.1175,	72.6265,	97.036	}
};
double mass_pres[6] =	{	0.0,	0.1,	0.25,	0.5,	0.75,	1 };
unsigned char mass_pres_size = 6;
double mass_res[4] =	{	4030.5,	4162.0,	4310.9,	4610.4 };
unsigned char mass_res_size = 4;
double pres, pres1;
// ������������ ���������� ��������
double Calculate_Pres(double u, double r);

#include "Mathematics.c"
#endif /* MATHEMATICS_H_ */
/*
* Проект малогабаритного датчика давления ЭнИ-12М
*
* Измерение температуры с помощью тензомоста. Термокомпенсация по 4-м точкам
* Конфигурация датчика по 1-wire
*
* MCU - MSP430I2041
* DAC - DAC161S997
* Internal freq - 16.384 MHz
* 
* Разработал: Цимбол А.И.
* Организация: ООО "Энергия-Источник"
* Дата начала: 06.06.2018
* Дата завершения: 22.06.2018
*/

//#include "io430.h"
#include "msp430.h"
//#include "math.h"

#define CPU_FREQ        	16384000		// 16.384 МГц Частота встроенного DCO
// Internal #define U_REF           	1.158           // Опорное напряжение для АЦП
#define U_REF           	1.3100           	// Опорное напряжение для АЦП
#define Addr_EEPROM_conf 	0xF000          // адрес начала записи/чтения настроек датчика
#define Addr_Segm1			0xF800 			// калибровочные коэффициенты са0-са23
#define Addr_Segm2			0xF400 			// серийный номер сенсора, ПД, ВП НП мин. предел сенсора, ед измерения сенсора
#define Addr_Segm3			0xF000 			// настройки датчика
#define Addr_Segm4			0xEC00 			// тэг, дескриптор, дата, сообщение
#define Addr_Segm5			0xE800 			// настройки условных единиц, сер.номер датчика для длинного фрейма
#define Addr_Segm6			0xE400 			// текущая конф датчика

#define Addr_cur_EEPROM_conf 0xE400       	// адрес начала записи/чтения текущей конфигурации датчика

//----Выводы ЦАП----------------------------------------------------------------

#define SCLK_DAC        6                   // Р2.6
#define SDI_DAC         2                   // Р2.2
#define SDO_DAC         0                   // Р2.0
#define CSB_DAC         1                   // Р2.1
#define PORT_DAC        P2OUT               // P2

#define TX_PIN 			3
#define RX_PIN 			2
#define PORT_UART		P1OUT

#define TX_IN 			P1SEL0 &= ~BIT3
#define WDT_START       (WDTPW+WDTTMSEL+WDTCNTCL+WDTSSEL+WDTIS0)

//----Коды/номера ошибок--------------------------------------------------------------------
#define ALL_OK			0 																	// Все ОК
#define PRES_OVER_HI	1 																	// Измеренное давление выше 1.1 Рв
#define PRES_OVER_LO	2 																	// Измеренное давление ниже 0.01 Рв, для ДИВ измеренное разряжение больше 1.01 Рв(-)
#define SET_ZERO_ERR	3 																	// При коррекции нуля от монтажного положения значение смещения нуля выходит за допустимые границы
#define ADC_ERR 		4 																	// Ошибка АЦП
#define EEPROM_ERR 		5 																	// Ошибка контрольной суммы ЕЕПРОМ
#define SENS_ERR		6																	// Ошибка сенсора

//----Уровни аварийной сигнализации (Namur)-------------------------------------------------
#define CRIT_ERR_LOW			3.6 														// Фиксированный токовый сигнал для подачи аварийного сигнала (критическая ошибка)
#define CURRENT_OUT_ALARM_LOW	3.8 														// Фиксированный токовый сигнал - ниже предельного значения
#define CURRENT_OUT_ALARM_HIGH	20.5 														// Фиксированный токовый сигнал - выше предельнего значения
#define CRIT_ERR_HIGH			22.5						                                // Фиксированный токовый сигнал для подачи аварийного сигнала (критическая ошибка)


//----Подпрограммы--------------------------------------------------------------------------
void I_ADC_write (unsigned char byteword);                                                  // запись данных в АЦП генератора тока
unsigned long I_ADC_read (void);                                                            // чтение данных из АЦП генератора тока
void DAC_send_data (unsigned char DAC_Addr, unsigned long DAC_data);                        // П/п отправки 24 бит данных от контроллера к ЦАП
void OUT_current (void);                                                                    // П/п устанавливает ток в токовой петле
void critical_error (void);                                                                 // П/П вывода номера ошибки на индикатор при аварийных ситуациях и установка аварийного сигнала в токовой петле
void Search_problem (void);                                                                 // П/П диагностики ошибок в датчике

void USART_Init (void);                                                                     // Инициализация USART
void Port_Mapping(void);                                                                    // Переназначение выводов контроллера (вывод ACLK на Р1.0)

void READ_4BYTE (unsigned char *ptr2, unsigned int addr);                                   // Чтение 4-х байт из EEPROM
unsigned char EEPROM_read(unsigned int adress);                                             // Чтение 1-го байта из EEPROM
void read_conf (void);                                                                      // Чтение настроек датчика из EEPROM
void CALL_read (void);                                                                      // Чтение калибровочных коэффициентов из EEPROM
void read_store_conf (void);                                                                // Чтение настроек сохраненных предприятием изготовителем датчика из EEPROM
void write_current_conf(void);																// П/П записи в EEPROM текущей конфигурации датчика
void EEPROM_write_pressure_cal (void);                                                      // П/П записи в EEPROM калибровки датчика по давлению * Сегмент 1 * 0xFDFF-0xFC00
void write_EEPROM_ASSEMBLY_NUMBER (unsigned char *prt);                                     // П/П записи Final Assembly Number * Сегмент 2 * 0xFBFF-0xFA00
void write_data_PD (unsigned char *prt);                                                    // П/П записи в EEPROM данных ПД * Сегмент 3 * 0xF9FF-0xF800
void write_EEPROM_TEG (unsigned char *prt);                                                 // П/П записи в EEPROM тега,дыты,описателя * Сегмент 4 * 0xF7FF-0xF600
void write_EEPROM_MESSAGE (unsigned char *prt);                                             // П/П записи в EEPROM сообщения * Сегмент 5 * 0xF5FF-0xF400
void write_EEPROM_SERIAL_PD (unsigned char *prt);                                           // П/П записи в EEPROM серийного номера ПД * Сегмент 6 * 0xF3FF-0xF200
void write_EEPROM_ZAVOD_NUMBER_DD (unsigned char *prt);                                     // П/П записи в EEPROM заводского номера ДД* Сегмент 7 * 0xF1FF-0xF000
void EEPROM_write_call (void);                                                              // П/П записи в EEPROM калибровочных коэффициентов * Сегмент C * 0x18FF-0x1880
void EEPROM_write_conf (void);                                                              // П/П записи в EEPROM конфигурации датчика * сегмент D * 0x187F-0x1800

float okruglenie (float abc, unsigned char num);                                            // П/П округления числа типа float
float perevod_v_davlenie (float znach);                                                     // П/П перевода в текущие ед. измерения
float perevod_v_uslovnie_edinici (float znach, char edinica);                               // П/П перевода в текущие ед. измерения
char proverka_delta_P (float delt_P);                                                       // П/П для проверки правильности коррекции нуля

void CRC16 (unsigned char *nach, unsigned char dlina);                                      // П/П расчета CRC16 значение полинома 0xA001 

//----Переменные----------------------------------------------------------------
// Ток в петле
unsigned int  b_current = 0x2AB4;                                               // Коэффициенты для вычисления кода ЦАП заданного
float k_current = 0.00036610;                                                   // значения выходного тока заданные по умолчанию

float loop_current = 4.0;                                                             // Ток в петле

unsigned char flag_i_out_auto = 1;
unsigned long b_ADC_mosta, r_ADC_mosta;                                                     // В них хранится значение кодов АЦП только для передачи их по HART протоколу

// Счетчики таймеров
int timA0count = 0;                                                                         // Переменная счетчик прерываний
int timD0count = 0;                                                                         // Переменная счетчик прерываний

unsigned char *Flash_ptr;                                                                   // Указатель на начало адреса с данными в EEPROM
unsigned char *CALL_ptr;                                                                    // Указатель на начало массива с калибровочными коэффициентами
float ca [24];                                                                              // Массив с калибровочными значениями
unsigned char edinica_izmer = 0x0C;                                                         // Единицы измерения

float R_mosta;                                                                              // Измеренное АЦП сопротивление моста
float temperatura=0, i_out=0, procent_diappazona = 0; 
double temp_u, u = 0.0;
double a0, a1, a2, a3, a4, a5, p = 0.0, f;
float davlenie;
unsigned long bb, r;                                                                        // код АЦП напряжения с сенсора и сопротивление моста

unsigned char flag_ADC, flag_ADC_IDLE=0;                                                    // Флаг измерения напряжения на выходе тензомоста, Флаг холостого измерения

unsigned long Skolzyashie_okno_R [8];
unsigned long *Rx_skolz_okno_R, *Tx_skolz_okno_R;    

unsigned char count_time;                                                                   // Счетчик таймера А0 для временных интервалов датчика температуры
unsigned char Term_time=0, Term_flag=1;                                                     // Счетчик времени датчика температуры, Флаг начала общения с датчикомс температуры                                                                     


unsigned char tok_signal_inv=0;                                                             // Выходной токовый сигнал 4-20 мА или 20-4 мА
unsigned char koren = 0x00;                                                                 // Корнеизвлекающая зависимость отключена    
unsigned char HART_disable = 0;                                                             // Разрешение конфигурации по HART
unsigned char magnit_key_enable=1;                                                          // Разрешение работы магнитной кнопки
unsigned char tmp_current_alarm, current_alarm = 0;                                         // Установка значения тока при появлении ошибки 
float diapazon_v = 1.000;                                                                   // Верхний поддиапазон
float diapazon_n = 0.000;                                                                   // Нижний поддиапазон
float tmp_diapazon_v, tmp_diapazon_n;                                                       // Используется при установке новых пределов по HART
float min_diapazon_PD, diapazon_n_PD;                                                       // используются при перенастройке пределов измерения
float delta_P = 0.0;                                                                          // Значение давления при калибровке 0

unsigned char N_integr;                                                                     // 32 измерения для интегрирования!не точно!
unsigned char N_integr_R = 8;
unsigned char N_integr_tmp=1;
unsigned char flag_R = 1;

float mastb;                                                                                // Масштабный коэффициент для перевода давления и единиц измерения

float tok_low_input, tok_hi_input;                                                          // Для расчета коэффициентов калибровки ЦАП через меню
float znach_vpi, znach_npi;                                                                 // используются для калибровки по давлению

float k_pressure = 1, b_pressure = 0;                                                       // Начальные значения коррекции характеристики по давлению
unsigned char n;
unsigned char i;

unsigned char crc_H, crc_L;                                                                 // Значения контрольной суммы

unsigned char ed_izmer_eeprom = 0x0C;                                                       // Код единицы измерения для значений перенастроек датчика 0х0С - кПа


unsigned char type_error = 0;                                                               // Тип (код-номер) ошибки
unsigned char flag_error = 0;                                                               // Флаг наличия ошибок в датчике

// Переменные необходимые на момент отладки
int tmp = 0, er_tmp = 0;                                                                    // Счетчик таймера А0 по которому сменяется текущий код ошибки +1; Переменная которая хранит текущий код ошибки

unsigned char modelPD [] = {0,'2','1','5','0','M'};											// Тип датчика давления 0x00 – ДИ, 0x01 – ДА, 0x02 – ДИВ, 0x03 – ДГ, 0x04 –ДД; Модель датчика

unsigned long u24_adc_H = 0;																// считанное напряжение с АЦП (старший байт)
unsigned long u24_adc_L = 0;																// считанное напряжение с АЦП (младший байт)
volatile unsigned long u24_adc = 0;															// считанное напряжение с АЦП
volatile unsigned int temp_adc = 0;             											// значение температуры взятое с АЦП

unsigned long u24_Umosta_H = 0;																// считанное напряжение с моста АЦП (старший байт)
unsigned long u24_Umosta_L = 0;																// считанное напряжение с моста АЦП (младший байт)
volatile unsigned long u24_Umosta = 0;														// считанное напряжение с моста АЦП
unsigned char flag_MEM = 0;                 												// флаг записи младшего байта АЦП

unsigned long buf_u24_adc[32];																// буфер значений выходного сигнала с тензика
unsigned char buf_u24_adc_index = 0;  														// индекс ячейки буфера
unsigned long buf_u24_Umosta[32];															// буфер значений напряжения питания моста
unsigned char buf_u24_Umosta_index = 0;														// индекс ячейки буфера
unsigned char flag_input_skolz_okno = 0;													// добавлено новое значение в скользящее окно
unsigned char mode = 0;

unsigned char ADCflagCompl = 0;	// флаг - завершено преобразование АЦП
unsigned char ADCtimeCount = 0; // пауза между преобразованиями АЦП


#include "Filtr.h"
#include "low_level_init.c"
//#include "Mathematics.h"
#include "USART_1WIRE.c"                                                        // Модуль USART
void main( void )
{
	// Stop watchdog timer to prevent time out reset
	WDTCTL = WDTPW + WDTHOLD;

	//----Настройка портов ввода/вывода-----------------------------------------

	P2DIR |= ((1<<SCLK_DAC) | (1<<SDI_DAC) | (1<<CSB_DAC));
	P2OUT &=~((1<<SCLK_DAC) | (1<<SDI_DAC) | (1<<CSB_DAC));

	P2OUT |= BIT0;																// SDO_DAC pull-up

	P2DIR |= (1<<7);															// вывод для отладки
	P2OUT &=~(1<<7);

	P1DIR |= ((1<<1) | (1<<0) | (1<<4) | (1<<5) | (1<<6) | (1<<7));				// неиспользуемые выводы на землю
	P1OUT &=~((1<<1) | (1<<0) | (1<<4) | (1<<5) | (1<<6) | (1<<7));

	P2DIR |= ((1<<3) | (1<<4) | (1<<5) | (1<<7));
	P2OUT &=~((1<<3) | (1<<4) | (1<<5) | (1<<7));

	// P1SEL1 |=BIT0;	//MCLK
    
	__low_level_init();
/*
	// Configure SMCLK = MCLK = 4MHz
    CSCTL1 |=  DIVM__4 | DIVS__4;     // MCLK = DCO/4, SMCLK = DCO/4 = 4,096V МГц
//	FCTL2 = FWKEY | FSSEL_1 | FN1 | FN3 | FN5;  								// MCLK/42 for Flash Timing Generator
    FCTL2 = FWKEY | FSSEL_1 | FN1 | FN3;  										// MCLK/10 for Flash Timing Generator
*/
/*	
	// Configure SMCLK = MCLK = 1MHz
    CSCTL1 |=  DIVM__16 | DIVS__16;     										// MCLK = DCO/16, SMCLK = DCO/16 = 1,024 МГц
    FCTL2 = FWKEY | FSSEL_1 | FN1 | FN0 ;  										// MCLK/3 for Flash Timing Generator
*/
	// Configure SMCLK = MCLK = 2MHz
    CSCTL1 |=  DIVM__8 | DIVS__8;     											// MCLK = DCO/16, SMCLK = DCO/8 = 2,048 МГц
    FCTL2 = FWKEY | FSSEL_1 | FN2 | FN0 ;  										// MCLK/5 for Flash Timing Generator


	//----Настройка таймера А0------------------------------------------------
	TA0CCTL0 = CCIE;                                                            // Разрешение прерываний по совпадению TA0CCR0
	TA0CCR0 = 72;                                                             // Значение, до которого считает таймер счетчик (3686400 / 4 / 2000 = 460,8 прерываний в секунду)
	TA0CTL = TASSEL_1 + MC_1 + ID_0;                                            // Тактирование от ACLK, режим счета - вверх, предделитель - 1
	
	//----Настройка таймера A1--------------------------------------------------
	TA1CCTL0 = CCIE;                                                            // Разрешение прерываний по совпадению TD0CCR0
	TA1CCR0 = 288;                                                              // Значение, до которого считает таймер счетчик (32000  / 288 = 111 прерываний в сек)
//    TA1CCR0 = 320;                                                              // Значение, до которого считает таймер счетчик (32000  / 320 = 100 прерываний в сек)
	TA1CTL = TASSEL_1 + MC_1 + TACLR;                                           // Тактирование от ACLK, режим счета - вверх, предделитель - 1

	//----Настройка ЦАП---------------------------------------------------------------------
	PORT_DAC |= (1<<CSB_DAC);                                                   // 1 на выходе CSB
	// Настройка ЦАП - отключение в регистре ERR_CONFIG ошибки по таймауту SPI и ошибки токовой петли
	DAC_send_data(0x05, 0x1403);
	OUT_current ();                                                             // устанавливаем значение выходного тока равным 4 мА

	USART_Init();
//	__delay_cycles(100000);														// задержка чтобы ЦАП успел одуматься, иначе при включении сходу не может установить 20 мА

	WDTCTL = WDT_ARST_1000;                                                     // Запуск сторожевого таймера период 1000 мс      

	//----Настройка АЦП---------------------------------------------------------
	// НАСТРОЙКА АЦП 24-БИТА
	SD24CTL &= ~(1<<SD24REFS);                         							// External ref
	//настройка коэф усиления АЦП напряжения с тензомоста
	// Для тензика ВИП макс значение АЦП в мВ составляет 1310.000
	unsigned char *ptrTypeTrans;                            					// Временный указатель на тип приемника давления в ЕЕПРОМ
    ptrTypeTrans = (unsigned char *) (Addr_Segm2 + 13);     					// Ставим указатель на адрес в ЕЕПРОМ с типом приемника давления
    if (*ptrTypeTrans == 0x04)                              					// Если датчик -ДД
		SD24INCTL0 |= SD24GAIN_16; 												// Коэф усиления АЦП напряжения с тензомоста - 16х		
	else if (*ptrTypeTrans == 0x03)                              				// Если датчик -ДГ
		SD24INCTL0 |= SD24GAIN_8; 												// Коэф усиления АЦП напряжения с тензомоста - 8х	 
	else if (*ptrTypeTrans == 0x02)                              				// Если датчик -ДИВ
		SD24INCTL0 |= SD24GAIN_4; 												// Коэф усиления АЦП напряжения с тензомоста - 4х	 
	else if (*ptrTypeTrans == 0x01)                              				// Если датчик -ДА
		SD24INCTL0 |= SD24GAIN_2; 												// Коэф усиления АЦП напряжения с тензомоста - 2х	 
	else
		SD24INCTL0 |= SD24GAIN_1;                           					// коэффициент усиления напряжения с тензомоста - 2х                               						// группа с каналом 1
	//конец настройки коэф усиления АЦП напряжения с тензомоста
	SD24CCTL0 |= SD24GRP | SD24SNGL;                       						// группа с каналом 2, единичное преобразование, разрешение прерывания0	
	SD24CCTL1 |= SD24GRP | SD24SNGL;                       						// группа с каналом 3, единичное преобразование, разрешение прерывания
	SD24INCTL2 |= SD24INCH_6;                           						// выбор датчика температуры
	SD24CCTL2 |= SD24DF | SD24SNGL | SD24IE;                       				// разрешение прерывания
	// КОНЕЦ НАСТРОЙКИ АЦП 24-БИТА
/*
	// НАСТРОЙКА АЦП 18-БИТ
	SD24CTL &= ~(1<<SD24REFS);                         							// External ref
	SD24CCTL0 |= SD24GRP |  SD24SNGL | SD24OSR_64;                       		// группа с каналом 2, единичное преобразование, разрешение прерывания0
	SD24CCTL1 |= SD24GRP |  SD24SNGL;                       					// группа с каналом 3, единичное преобразование, разрешение прерывания
	SD24INCTL2 |= SD24INCH_6;                           						// выбор датчика температуры
	SD24CCTL2 |= SD24DF |  SD24SNGL | SD24IE;                       			// разрешение прерывания
	// КОНЕЦ НАСТРОЙКИ АЦП 18-БИТА
*/
	//----ЧТЕНИЕ НАСТРОЕК ДАТЧИКА ИЗ EEPROM-------------------------------------------------
	CALL_ptr = (unsigned char *) Addr_EEPROM_conf;
	CRC16 (CALL_ptr, 126);                                                                  // Расчитываем значение CRC для блока данных с настройками датчика
	CALL_ptr = (unsigned char *) Addr_EEPROM_conf + 126;

	if (crc_H == *CALL_ptr)                                                                 // При включении делаем проверку CRC памяти с настройками датчика
	{
		CALL_ptr += 1;
		if (crc_L == *CALL_ptr)
		{
			read_conf ();                                                                   // Если контрольная сумма совпадает - чтение настроек датчика из EEPROM
		}
		else                                                                                // Если контрольная сумма EEPROM не совпадает - то выводим на индикатор сообщение об ошибке
		{                                                                                   // Err-6
			flag_error = 1;
			type_error = EEPROM_ERR; 
		}
	}
	else                                                                                    // Установить тип-код ошибки номер 6
	{
		flag_error = 1;
		type_error = EEPROM_ERR;
	}

	//----Считать коэффициенты корректировки характеристики по давлению---------------------
	CALL_ptr = (unsigned char *) 0xF413 + 8;                                                // проверка корректной записи калибровки по далению
	if (*CALL_ptr != 0x00)                                                                  // если это первое включение после прошивки - записываем коэффициенты принятые по умолчанию (k=1, b=0)
	{
		EEPROM_write_pressure_cal ();                                                       //
	}
	else                                                                                    // если запись уже была проведена, считываем значение коэффициентов корректировки характеристики по давлению
	{
		CALL_ptr = (unsigned char *) & k_pressure;
		READ_4BYTE (CALL_ptr, 0xF413);                                                      //

		CALL_ptr = (unsigned char *) & b_pressure;
		READ_4BYTE (CALL_ptr, 0xF417);                                                      //
	}
	//--------------------------------------------------------------------------------------

	CALL_read ();                                                                           // чтение калибровочных коэффициентов

	CALL_ptr = (unsigned char *) & mastb;
	READ_4BYTE (CALL_ptr, 0xF401);                                                          //

	CALL_ptr = (unsigned char *) 0xF400;
	if (*CALL_ptr == 0x0C) mastb = mastb * 1;                                               // чтение масштабного коэффициента (верхнего предела измерения ПД) для расчета давления
	else mastb = mastb * 1000;                                                              // если значение указано в МПа, тогда домножаем множитель на 1000

	CALL_ptr = (unsigned char *) & znach_vpi;
	READ_4BYTE (CALL_ptr, 0xF401);                                                          // считываем значение пределов измерения приемника давления

	CALL_ptr = (unsigned char *) & znach_npi;
	READ_4BYTE (CALL_ptr, 0xF405);                                                          // считываем значение пределов измерения приемника давления

	diapazon_n_PD = znach_npi / znach_vpi;
	min_diapazon_PD = (1.0 - diapazon_n_PD)/25.5;

	N_integr = (unsigned char) (damping_value / 0.0786) + 1;                                // рассчитываем значение кол-ва отсчетов для интегрирования
	rc = (int) (18 * damping_value);																// 
	for (int i = 0; i < 10; i++)
	{
	    if((2 << i) >= rc)
	    {
	        rc = i;
	        break;
	    }
	}
	if(rc < 2)      rc = 2;
	if(rc > 8)      rc = 8;
	
	if (DEV_ID != 0x00)                                                                     // если адрес устройства не нулевой - значит включен режим моноканала
	{
		flag_i_out_auto = 0;                                                                // входим в режим фиксированного тока
		loop_current = 4.000;
		OUT_current ();                                                                     // устанавливаем значение выходного тока равным 4 мА
	}

	_EINT();	                                                                			// Глобальное разрешение прерываний
	Tx_skolz_okno_R = Rx_skolz_okno_R = (unsigned long *)& Skolzyashie_okno_R;              // указатели скользящего окна - на начало буфера (усреднение измерений)
	
	ptr_mfb = (unsigned long *)& median_filtr_buf;											// указатель на начало медианного фильтра входных значений с тензика
	SD24CCTL2 |= SD24SC;                                									//Установка бита начала преобразования

	while(1)
	{
		WDTCTL = WDTPW + WDTCNTCL;                                              			// сброс сторожевого таймера	
		if (CDR_END_HART == 1) parser_USART();                                  			// Если установлен флаг приема нового кадра, идем разбирать принятую посылку.
		WDTCTL = WDTPW + WDTCNTCL;                                              			// сброс сторожевого таймера

		if(flag_input_skolz_okno == 1)
		{
			if (mode == 0)
			{
				
				//  Sensor's temperature coefficient is 2.158mV/K (from datasheet)
				//  Sensor's offset voltage ranges from -100mv to 100mV (assume 0)
				//  Vsensor = 1.32mV * DegK + Vsensor_offset (assuming 0mv for offset)
				//  Vsensor = (SD24MEM0)/32767 * Vref(mV)
				//  DegK = (SD24MEM0 * 1200)/32767/2.158 = (SD24MEM0 * 1200)/70711
				//  DegC =  DegK - 273
				//	temperatura = ((((float)temp_adc * 1158)/70711) - 273);                    			// расчет температуры в градусах цельсия от внутреннего опорного

				// для тензика
				if(R_mosta > 3500)
					temperatura = ((((float)temp_adc * 1355)/70711) - 273);                    			// расчет температуры в градусах цельсия от внешнего опорного
				else
					//для сенсора китайского
					temperatura = ((((float)temp_adc * 1800)/70711) - 273);                    			// расчет температуры в градусах цельсия от внешнего опорного
				temperatura = (int) (temperatura * 10);
				temperatura = temperatura/10;

				/* расчет относительно сопротивления диагонали
				float koef = 4667 / (1800 + R_mosta);
				int koef1 = (int)((koef) * 1800);
				temperatura = ((((float)temp_adc * koef1)/70711) - 273);                    			// расчет температуры в градусах цельсия от внешнего опорного
				temperatura = (int) (temperatura * 10);
				temperatura = temperatura/10;
				*/

				b_ADC_mosta = bb;                                            // сохраняем код АЦП измеренного давления для передачи по HART протоколу
// 24 бита 
				// вычисление напряжения на выходе датчика 
				if (bb >= 0x800000) 
				{ 
					bb = bb - 0x800000;                                      //
					u = 1000*((U_REF/0x800000)*bb);                          // измеренное напряжение в мВ
				} 
				else 
				{ 
					bb = 0x800000 - bb; 
					u = 1000*(-(U_REF/0x800000)*bb);                         // измеренное напряжение в мВ
				} 
 
/* 
// 21 бит 
				// вычисление напряжения на выходе датчика 
				if (bb >= 0x0FFFFF) 
				{ 
					bb = bb - 0x0FFFFF;                                      //
					u = 1000*((U_REF/0x0FFFFF)*bb);                          // измеренное напряжение в мВ
				} 
				else 
				{ 
					bb = 0x0FFFFF - bb; 
					u = 1000*(-(U_REF/0x0FFFFF)*bb);                         // измеренное напряжение в мВ
				} 
*/ 
// 18 бит 
/*				 
				// вычисление напряжения на выходе датчика 
				if (bb >= 0x01FFFF) 
				{ 
					bb = bb - 0x01FFFF;                                      //
					u = 1000*((U_REF/0x01FFFF)*bb);                          // измеренное напряжение в мВ
				} 
				else 
				{ 
					bb = 0x01FFFF - bb; 
					u = 1000*(-(U_REF/0x01FFFF)*bb);                         // измеренное напряжение в мВ
				} 
*/ 
				// не делим напряжение с сенсора
				//u = u/2;													 // делим полученное напряжение с тензика пополам из за коэф усиления 2		
				//
				i = N_integr_R;
				r = 0;
				while (i)
				{
					r = r + *Rx_skolz_okno_R;                                // суммируем выборки
					Rx_skolz_okno_R += 1;                                    //
					i = i - 1;
				}
				r = r / N_integr_R;                                          // делим результат на кол-ко выборок
				Rx_skolz_okno_R = (unsigned long *)& Skolzyashie_okno_R;     // указатель чтения скользящего окна - на начало буфера

				r_ADC_mosta = r;   							                 // сохраняем код АЦП для передачи по HART протоколу
				R_mosta = ((1800.0/0x7FFFFF) * (r-0x800000) * 3);
				R_mosta = okruglenie (R_mosta, 1);

				// Самодиагностика
				if (type_error == EEPROM_ERR)                                 // При ошибке EEPROM периодически выполняем П/П вывода аварийного тока
				{
					loop_current = CRIT_ERR_LOW;
					OUT_current ();                                           // выдаем на выходе расчетное значение тока
				}
				else
				{
					if (b_ADC_mosta < 0x0020 || b_ADC_mosta > 0xFFFFE0)       // если код АЦП имеет крайние значения - неисправность сенсора
					{
						loop_current = CRIT_ERR_LOW;
						OUT_current ();                                       // выдаем на выходе расчетное значение тока
					}
					else
					{
					
						f = R_mosta * R_mosta;    // пересчитываем коэффициенты для нового сопротивления тензомоста при новой температуре
						a0 = ca[0]  + ca[1]  * R_mosta + ca[2]  * f + ca[3]  * f * R_mosta;
						a1 = ca[4]  + ca[5]  * R_mosta + ca[6]  * f + ca[7]  * f * R_mosta;
						a2 = ca[8]  + ca[9]  * R_mosta + ca[10] * f + ca[11] * f * R_mosta;
						a3 = ca[12] + ca[13] * R_mosta + ca[14] * f + ca[15] * f * R_mosta;
						a4 = ca[16] + ca[17] * R_mosta + ca[18] * f + ca[19] * f * R_mosta;
						a5 = ca[20] + ca[21] * R_mosta + ca[22] * f + ca[23] * f * R_mosta;

						mode = 1;
					}

				}
			}
			else
			{
				// вычисление давления
				temp_u = u;
				p = a0 + a1 * u;
				temp_u = temp_u * u;
				p = p + a2 * temp_u;
				temp_u = temp_u * u;
				p = p + a3 * temp_u;
				temp_u = temp_u * u;
				p = p + a4 * temp_u;
				temp_u = temp_u * u;
				p = p + a5 * temp_u;
							
				//p = Calculate_Pres(u, R_mosta); // расчет кусочно линейной

				p = p - delta_P;
				davlenie = perevod_v_davlenie (p);

				//procent_diappazona = (p - diapazon_n)/((diapazon_v - diapazon_n) / 100);  // считаем процент диапазона
				/*
				if (koren)
				{
					if (p >= 0.8 * (diapazon_v / 100))                              // 0,8% от начала дапазона делаем линейным, а все что выше - делаем корнеизвлечение
					{
						i_out = 4 + 16 * sqrt (p / diapazon_v);                     // корнеизвлекающая зависимость
					}
					else
					{
						if (p <= 0.6 * (diapazon_v /100))
						{
							i_out = 4 + p * (16.0 / diapazon_v);                    // от 0 до 0,6% от диапазона и при отрицательном давлении линеаризация по одному ленейному закону
						}
						else
						{
							i_out = 4.096 + ((p / diapazon_v) - 0.006) * 667.5;     // при давлении от 0,6 до 0,8% и при отрицательном - делаем линейную зависимость с другим наклоном.
						}
					}
				} 
				else*/
				{
					i_out = 4 + (p - diapazon_n) * (16.0 / (diapazon_v - diapazon_n)); // считаем значение выходного тока при линейной зависимости
				}
/*
				if (tok_signal_inv)
				{
					i_out = 24 - i_out;                                             // инвертируем токовый сигнал если установлен ток 20-4
				}
*/
				if (i_out < CURRENT_OUT_ALARM_LOW)  i_out = CURRENT_OUT_ALARM_LOW;	// Проверяем значение выходного тока на превышение макс и мин значения согласно NAMUR NE43
				if (i_out > CURRENT_OUT_ALARM_HIGH) i_out = CURRENT_OUT_ALARM_HIGH;

				if (flag_i_out_auto)                                                // если находимся в режиме не фиксированного тока
				{
					loop_current = i_out;                                           //
					OUT_current ();                                                 // выдаем на выходе расчетное значение тока
				}
				flag_input_skolz_okno = 0;											// сброс флага наличия нового значения в скользящем окне

				mode = 0;
			}
		}
	}

}


//==========================================================================================
//  П/п обработки прерываний по таймеру A0 (Индикация светодиода)
//==========================================================================================
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A0 (void)
{
	if(ADCflagCompl)
	{
		ADCflagCompl = 0;
		
		SD24CCTL2 |= SD24SC;
	}
}


//==========================================================================================
//  П/п обработки прерываний по таймеру A1 (Интервалы для HART-протокола)
//==========================================================================================
#pragma vector=TIMER1_A0_VECTOR
__interrupt void Timer1_A0 (void)
{   
	count_time_HART += 1;
	if (count_time_HART == time_8_HART) { CDR_END_HART = 1;}
	if (count_time_HART == 255) count_time_HART = 254;	
}


//==========================================================================================
//  П/п обработки прерываний от АЦП
//==========================================================================================
#pragma vector=SD24_VECTOR
__interrupt void SD24_ISR(void)
{
    switch (__even_in_range(SD24IV,SD24IV_SD24MEM3)) {
        case SD24IV_NONE: break;
        case SD24IV_SD24OVIFG: break;
        case SD24IV_SD24MEM0:
        {
        	break;
        }     
        case SD24IV_SD24MEM1:
        {
        	break;
        } 
        case SD24IV_SD24MEM2:
        {
        	if(flag_MEM == 0)
			{    
// 24-бит
				// выходной сигнал с моста
				u24_adc_H = SD24MEM0;              				
/* 18-бит
				// выходной сигнал с моста
				u24_adc_H = 0x00000000;
				u24_adc_H = SD24MEM0;              
				u24_adc_H = u24_adc_H << 2;                                    	// сдвиг на 8 бит чтобы записать младшие 16 бит
				u24_adc_H &= 0x00FF0000;                                    	// обнуление младших 16 бит 
				SD24CCTL0 |= SD24LSBACC;                                 		// установка бита записи младших бит АЦП 0
*/
				// напряжение питания моста
				u24_Umosta_H = SD24MEM1;              
				u24_Umosta_H = u24_Umosta_H << 8;                                   // сдвиг на 8 бит чтобы записать младшие 16 бит
				u24_Umosta_H &= 0x00FFFF00;                                    	// обнуление младших 16 бит 
				SD24CCTL1 |= SD24LSBACC;                                 		// установка бита записи младших бит АЦП 0
				SD24CCTL0 |= SD24LSBACC;                                 		// установка бита записи младших бит АЦП 0
				flag_MEM = 1;                                            		// установка флага, чтобы в следующем прерывании снять младшие бит с АЦП  
			}
			else
			{
				// выходной сигнал с моста
				u24_adc_L = SD24MEM0;  
				u24_adc_H = u24_adc_H << 8;                                    	// сдвиг на 8 бит чтобы записать младшие 16 бит
				u24_adc_L &= 0x000000ff;
				u24_adc = u24_adc_H | u24_adc_L;                                // склеивание страшей и младшей части с АЦП 0
				// Фильтр значений
				*ptr_mfb = u24_adc;												// добавление значения в кольцевой медианный фильтр (размер 3)
				ptr_mfb +=1;
				if(ptr_mfb > ((unsigned long *)& median_filtr_buf + 2))
					ptr_mfb = (unsigned long *)& median_filtr_buf;
				// Цифровой RC- фильтр
				flag_input_skolz_okno = 1;										// в скользящее окно добавлено новое значение
				unsigned long val;
				val = rc_filtr (middle_of_3(median_filtr_buf[0], median_filtr_buf[1], median_filtr_buf[2]), rc);
				bb = val;
				// конец цифрового RC- фильтра
				
				// напряжение питания моста
				u24_Umosta_L = SD24MEM1;
				u24_Umosta_L &= 0x000000ff;
				u24_Umosta = u24_Umosta_H | u24_Umosta_L;                       // склеивание страшей и младшей части с АЦП 0

				if(buf_u24_Umosta_index < 8)									// пока не набрали 16 значений	
				{
					buf_u24_Umosta[buf_u24_Umosta_index] = u24_Umosta;			// заполняем буфер значениями напряжения питания моста 
					buf_u24_Umosta_index +=1;
				}
				else															// если заполнили
				{
					buf_u24_Umosta_index = 0;										
					unsigned long u24filter = 0;								// временная переменная накопитель значений напряжения питания моста
					while(buf_u24_Umosta_index < 8)								// суммируем значения напряжения питания моста
					{
						u24filter += buf_u24_Umosta[buf_u24_Umosta_index];
						buf_u24_Umosta_index +=1;
					}
					buf_u24_Umosta_index = 0;                            
					*Tx_skolz_okno_R = (u24filter >> 3);                            // получ. знач. в скользящее окно (делим сумму значений напряжения питания моста на 8 - находим среднее)
					Tx_skolz_okno_R += 1;
					if (Tx_skolz_okno_R > (Skolzyashie_okno_R + N_integr_R -1))     // если записали в скользящее окно N_integr занчений
					{
						Tx_skolz_okno_R = (unsigned long *)& Skolzyashie_okno_R;    // указатель записи скользящего окна - на начало буфера
					}
				}

				SD24CCTL1 &= ~SD24LSBACC;                                		// сброс бита записи младших бит АЦП в следующем прерывании будет запись сташего бита 
				SD24CCTL0 &= ~SD24LSBACC;                                		// сброс бита записи младших бит АЦП в следующем прерывании будет запись сташего бита
				flag_MEM = 0;                                            		// сбос флага младших бит АЦП
			} 
			temp_adc = SD24MEM2;												// температура
			ADCflagCompl = 1;
        	break;
        } 
        case SD24IV_SD24MEM3: break;
        default: break;
    }
}


//==========================================================================================
// П/п записи в ЦАП
//==========================================================================================
void DAC_send_data (unsigned char DAC_Addr, unsigned long DAC_data)
{
	long SPI_data = 0x00000000;                                                 // Переменная акумулятор для отправки по SPI в ЦАП
	SPI_data += DAC_Addr;                                                       // Добавляем в переменную-аккумулятор 8 бит адреса для отправки по SPI
	SPI_data = SPI_data << 16;                                                  // Сдвигаем данные на 16 разрядов, для данных
	SPI_data += DAC_data;                                                       // Добавляем в переменную-аккумулятор 16 бит данных для отправки по SPI
	PORT_DAC &= ~(1<<CSB_DAC);                                                  // Устанавливаем в 0 линию CSB
	_NOP(); //__delay_cyles(1680);
	for (unsigned char dac_i = 0; dac_i < 24; dac_i++)
	{
		if (SPI_data  & 0x800000)                                               // Смотрим что находится в 24 бите передаваемых данных, если 1, то
		{
			PORT_DAC |= (1<<SDI_DAC);                                           // Выдаем лог. 1 на SDI ЦАП
		}
		else
		{
			PORT_DAC &= ~(1<<SDI_DAC);                                          // Выдаем лог. 0 на SDI ЦАП
		}
		PORT_DAC |= (1<<SCLK_DAC);                                              // Тактирование CLK ЦАП Передний фронт
		_NOP(); //__delay_cyles(1680);
		PORT_DAC &= ~(1<<SDI_DAC);                                              // Выдаем лог. 0 на SDI ЦАП 
		_NOP(); //__delay_cyles(1680);
		PORT_DAC &= ~(1<<SCLK_DAC);                                             // Тактирование CLK ЦАП Задний фронт
		_NOP(); //__delay_cyles(1680);
		SPI_data = SPI_data << 1;                                               // Сдвигаем данные влево на 1 разряд
	}
	_NOP(); 
	PORT_DAC |= (1<<CSB_DAC);                                                   // Устанавливаем в 1 линию CSB
	PORT_DAC &= ~(1<<SDI_DAC);                                                  // Выдаем лог. 0 на SDI ЦАП
}


//==========================================================================================
//  П/П управления ЦАП  
//==========================================================================================
void OUT_current (void)
{
	unsigned long DAC_code_current = 0;                                         // Переменная для хранения кода ЦАП для значения тока
	// Для случая если вдруг ЦАП перезагрузился от помехи, а MCU нет
	DAC_send_data(0x05, 0x1403);                                                // Настройка ЦАП - отключение в регистре ERR_CONFIG ошибки по таймауту SPI и ошибки токовой петли 

//	if (loop_current < 3.50) loop_current = 3.50;                               // делаем проверку и коррекцию заданного значения тока
//	if (loop_current > 23.0) loop_current = 23.0;                               //

	DAC_code_current = b_current + (long)((loop_current - 4.0)/k_current);      // получаем код для загрузки в ЦАП
	DAC_send_data(0x04, DAC_code_current);                                      // Отправляем код в ЦАП
}


//==========================================================================================
// Чтение 4-х байтного числа из EEPROM 
//==========================================================================================
void READ_4BYTE (unsigned char *ptr2, unsigned int addr)
{
	Flash_ptr = (unsigned char *)addr;
	*ptr2 = *Flash_ptr;                                                                     // |
	ptr2 += 1; Flash_ptr += 1;                                                              // |
	*ptr2 = *Flash_ptr;                                                                     // |
	ptr2 += 1; Flash_ptr += 1;                                                              // | считываем 4 байта подряд с указанного адреса
	*ptr2 = *Flash_ptr;                                                                     // |
	ptr2 += 1; Flash_ptr += 1;                                                              // |
	*ptr2 = *Flash_ptr;                                                                     // |
}


//==========================================================================================
// Чтение 1 байта из ЕЕPROM
//==========================================================================================
unsigned char EEPROM_read(unsigned int adress)
{
	Flash_ptr = (unsigned char *)adress;                                                    // адресное пространство flash 0x8000 - 0xFFFF
	return *Flash_ptr;
}


//==========================================================================================
// Чтение калибровочных коэффициентов
//==========================================================================================
void CALL_read (void)
{
	volatile unsigned char i_e;                                                             //
	// Код для понинома
	CALL_ptr = (unsigned char *) & ca;                                                      // Присваиваем указатель на начало массива с калибровочными коэффициентами
	Flash_ptr = (unsigned char *) Addr_Segm1;                                               // Указываем начальный адрес EEPROM, где храняться калибровочные коэффициенты 
	for (i_e=0; i_e<96; i_e++)
	{
		*CALL_ptr++ = *Flash_ptr++;                                                         // переписываем коэффициенты
	}
	// Конец код для полинома

	/* 
	// Код для кусочно линейной
	Flash_ptr = (unsigned char *) Addr_Segm1;                                               // Указываем начальный адрес EEPROM, где храняться калибровочные коэффициенты 
	mass_pres_size = *Flash_ptr;
	Flash_ptr += 1;
	mass_res_size = *Flash_ptr;
	Flash_ptr += 1;
        CALL_ptr = (unsigned char *) & mass_pres;                                              // Присваиваем указатель на начало массива с калибровочными коэффициентами
	for (i_e=0; i_e<24; i_e++)
	{
		*CALL_ptr++ = *Flash_ptr++;                                                         // переписываем коэффициенты
	}
        CALL_ptr = (unsigned char *) & mass_res;                                              // Присваиваем указатель на начало массива с калибровочными коэффициентами
	for (i_e=0; i_e<16; i_e++)
	{
		*CALL_ptr++ = *Flash_ptr++;                                                         // переписываем коэффициенты
	}
	CALL_ptr = (unsigned char *) & mass_value;                                              // Присваиваем указатель на начало массива с калибровочными коэффициентами
	for (i_e=0; i_e<96; i_e++)
	{
		*CALL_ptr++ = *Flash_ptr++;                                                         // переписываем коэффициенты
	}
	// Конец кода кусочно линейной
	*/
}


//==========================================================================================
// Чтение настроек датчика
//==========================================================================================
void read_conf (void)
{
	tok_signal_inv =EEPROM_read (Addr_EEPROM_conf + 1);
	edinica_izmer = EEPROM_read (Addr_EEPROM_conf + 2);
	koren =         EEPROM_read (Addr_EEPROM_conf + 3);
	DEV_ID =        EEPROM_read (Addr_EEPROM_conf + 4);
	PREAMBULE =     EEPROM_read (Addr_EEPROM_conf + 5);
	HART_disable =  EEPROM_read (Addr_EEPROM_conf + 6);
	magnit_key_enable = EEPROM_read (Addr_EEPROM_conf + 7);
	current_alarm = EEPROM_read (Addr_EEPROM_conf + 30);

	CALL_ptr = (unsigned char *) & diapazon_v;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 8);

	CALL_ptr = (unsigned char *) & diapazon_n;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 12);

	CALL_ptr = (unsigned char *) & damping_value;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 16);

	CALL_ptr = (unsigned char *) & delta_P;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 20);

	CALL_ptr = (unsigned char *) & k_current;
	READ_4BYTE (CALL_ptr, Addr_EEPROM_conf + 24);

	CALL_ptr = (unsigned char *) & b_current;
	*CALL_ptr = EEPROM_read (Addr_EEPROM_conf + 28);
	CALL_ptr += 1;
	*CALL_ptr = EEPROM_read (Addr_EEPROM_conf + 29);

	CALL_ptr = (unsigned char *) & UIN + 2;                                                 // чтение серийного номера датчика
	*CALL_ptr = EEPROM_read (0xE800 + 0);
	CALL_ptr += 1;
	*CALL_ptr = EEPROM_read (0xE800 + 1);
	CALL_ptr += 1;
	*CALL_ptr = EEPROM_read (0xE800 + 2);
}


//==========================================================================================
// П/П записи в EEPROM калибровки датчика по давлению * Сегмент 2 * 0xF7FF-0xF400
//==========================================================================================
void EEPROM_write_pressure_cal (void)
{
	// сперва копируем в оперативу ед. измер ПД, ВПИ ПД, НПИ ПД, Мин диап. ПД, Модель ПД
	unsigned char j = 0;
	unsigned char tmpbuf[19];																// временный буфер
    Flash_ptr = (unsigned char *) Addr_Segm2;                                 				// указатель на ячейку EEPROM (сегмент 2)
    while (j < 19)                                                          				// считываем сообщение
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }
    // копируем в оперативу серийный номер ПД
    unsigned char tmpbuf1[3];																// временный буфер
    Flash_ptr = (unsigned char *) (Addr_Segm2 + 28);                                 		// указатель на ячейку EEPROM (сегмент 2)
    j = 0;
    while (j < 3)                                                          					// считываем сообщение
    {
        tmpbuf1[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog
	_DINT();                                                                                // запрет всех прерываний
	Flash_ptr = (unsigned char *) (Addr_Segm2 + 19);                                        // Указываем на начало сегметна записи
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
//	FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных

	// Запись данных в сегмент
	CALL_ptr = (unsigned char *) & k_pressure;                                              // коэффициент корректировки наклона характеристики по давлению
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) &  b_pressure;                                             // коэффициент корректировки смещения характеристики по давлению
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	*Flash_ptr = 0x00;                                                                      // записываем метку указывающую что запись коэффициентов была проведена. (проверяется при включении и если нет метки - то запивывается коэффициенты по умолчанию)
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	// восстанавливаем ед. измер ПД, ВПИ ПД, НПИ ПД, Мин диап. ПД, Модель ПД
	Flash_ptr = (unsigned char *) Addr_Segm2;                                 				// указатель на ячейку EEPROM (сегмент 2)	
	for (j=0; j<19; j++)
	{
		*Flash_ptr++ = tmpbuf[j];                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	// восстанавливаем серийный номер ПД
	Flash_ptr = (unsigned char *) (Addr_Segm2 + 28);                                 		// указатель на ячейку EEPROM (сегмент 2)	
	for (j=0; j<3; j++)
	{
		*Flash_ptr++ = tmpbuf1[j];                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	 WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();
}


//==========================================================================================
// П/П записи Final Assembly Number * Сегмент 5 * 0xEBFF-0xE800
//==========================================================================================
void write_EEPROM_ASSEMBLY_NUMBER (unsigned char *prt)
{
	// сперва копируем в оперативу серийный номер датчика
	unsigned char j = 0;
	unsigned char tmpbuf[3];																// временный буфер
    Flash_ptr = (unsigned char *) Addr_Segm5;                                 				// указатель на ячейку EEPROM (сегмент 5)
    while (j < 3)                                                          				
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog 
	_DINT();                                                                                // запрет всех прерываний
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) (Addr_Segm5 + 3);                                         // Указываем на начало сегметна записи
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
//	FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных

	for (i_e=0; i_e<3; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              // Записывыем по байтно серийный номер ПД
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	// восстанавливаем серийный номер датчика
	Flash_ptr = (unsigned char *) (Addr_Segm5);                                 			// указатель на ячейку EEPROM (сегмент 5)	
	for (j=0; j<3; j++)
	{
		*Flash_ptr++ = tmpbuf[j];                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	 WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();
}


//==========================================================================================
// П/П записи в EEPROM данных ПД * Сегмент 2 * 0xF7FF-0xF400
//==========================================================================================
void write_data_PD (unsigned char *prt)
{
	// сперва копируем в оперативу калибровочные коэф по давлению k_pressure, b_pressure, метка, серийный номер ПД
	unsigned char j = 0;
	unsigned char tmpbuf[12];																// временный буфер
    Flash_ptr = (unsigned char *) (Addr_Segm2 + 19);                                 		// указатель на ячейку EEPROM (сегмент 2)
    while (j < 12)                                                          				
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog
	_DINT();                                                                                // запрет всех прерываний
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) Addr_Segm2;                                               // Указываем на начало сегмента записи
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
//	FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных

	*Flash_ptr = *prt;                                                                      // единицы измерения
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash
	prt += 4;   
	Flash_ptr += 1;

	for (i_e=0; i_e<4; i_e++)
	{
		*Flash_ptr++ = *prt--;                                                              // Записывыем по байтно данные верхний диапазон
	}
	prt += 8;
	for (i_e=0; i_e<4; i_e++)
	{
		*Flash_ptr++ = *prt--;                                                              // Записывыем по байтно данные нижний диапазон
	}
	prt += 8;
	for (i_e=0; i_e<4; i_e++)
	{
		*Flash_ptr++ = *prt--;                                                              // Записывыем по байтно данные минимальный подиапазон
	}

	unsigned char *ptrModel;																// Временная переменная указатель для записи типа и модели ПД
	ptrModel = (unsigned char *) & modelPD;													// Указатель на начало массива вида 02550М
	for (i_e=0; i_e<6; i_e++)
	{
		*Flash_ptr++ = *ptrModel++;
		while(!(FCTL3 & WAIT));
	}

	// восстанавливаем калибровочные коэф по давлению k_pressure, b_pressure,метка, серийный номер ПД
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<12; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	 WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();
}


//==========================================================================================
// П/П записи в EEPROM тега,дыты,описателя * Сегмент 4 * 0xEFFF-0xEC00
//==========================================================================================
void write_EEPROM_TEG (unsigned char *prt)
{
	// сперва копируем в оперативу сообщение
	unsigned char j = 0;
	unsigned char tmpbuf[24];																// временный буфер
    Flash_ptr = (unsigned char *) (Addr_Segm4 + 21);                                 		// указатель на ячейку EEPROM (сегмент 4)
    while (j < 24)
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog
	_DINT();                                                                                // запрет всех прерываний
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) Addr_Segm4;                                               // Указываем на начало сегметна записи
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
//	FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных
	
	// Записывыем по байтно Тэг, описатель, дату
	for (i_e=0; i_e<21; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	// восстанавливаем обратно сообщение
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<24; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	 WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();
}


//==========================================================================================
// П/П записи в EEPROM сообщения * Сегмент 4 * 0xEFFF-0xEC00
//==========================================================================================
void write_EEPROM_MESSAGE (unsigned char *prt)
{
	// сперва копируем в оперативу тег, дату, дескриптор
	unsigned char j = 0;
	unsigned char tmpbuf[21];																// временный буфер
    Flash_ptr = (unsigned char *) Addr_Segm4;                           	      			// указатель на ячейку EEPROM (сегмент 4)
    while (j < 21)                                                          				// считываем сообщение
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog
	_DINT();                                                                                // запрет всех прерываний
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) (Addr_Segm4 + 21);                                        // Указываем на начало сегметна записи
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
//	FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных

	// Записывыем по байтно MESSAGE
	for (i_e=0; i_e<24; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	// восстанавливаем тег, дату, дескриптор
	Flash_ptr = (unsigned char *) Addr_Segm4;                                        		// Указываем на начало сегметна записи
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<21; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	 WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();
}


//==========================================================================================
// П/П записи в EEPROM серийного номера ПД * Сегмент 2 * 0xF7FF-0xF400
//==========================================================================================
void write_EEPROM_SERIAL_PD (unsigned char *prt)
{
	// сперва копируем в оперативу Верхний и нижний пределы, минимальный диапазон сенсора, ед. Измерения, Калибровочные коэффищиенты по давлению k_pressure, b_pressure
	unsigned char j = 0;
	unsigned char tmpbuf[28];																// временный буфер
    Flash_ptr = (unsigned char *) Addr_Segm2;                                 				// указатель на ячейку EEPROM (сегмент 4)
    while (j < 28)                                                          				
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog
	_DINT();                                                                                // запрет всех прерываний
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) (Addr_Segm2 + 28);                                        // Указываем на начало сегметна записи
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                      						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
	//FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных

	for (i_e=0; i_e<3; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              // Записывыем по байтно серийный номер ПД
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	// восстанавливаем все обратно из оперативы
	Flash_ptr = (unsigned char *) Addr_Segm2;                                 				// указатель на ячейку EEPROM (сегмент 4)
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<28; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	 WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();                                
}


//==========================================================================================
// П/П записи в EEPROM заводского номера ДД* Сегмент 5 * 0xFBFF-0xE800
//==========================================================================================
void write_EEPROM_ZAVOD_NUMBER_DD (unsigned char *prt)
{
	// сперва копируем в оперативу Final Assembly Number
	unsigned char j = 0;
	unsigned char tmpbuf[3];																// временный буфер
    Flash_ptr = (unsigned char *) (Addr_Segm5 + 3);                                 		// указатель на ячейку EEPROM (сегмент 5)
    while (j < 3)                                                          				
    {
        tmpbuf[j] = *Flash_ptr;                                                      
        Flash_ptr += 1;
        j += 1;                                               
    }

	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog
	_DINT();                                                                                // запрет всех прерываний
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) Addr_Segm5;                                               // Указываем на начало сегметна записи
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                       						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
	//FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных

	for (i_e=0; i_e<3; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              // Записывыем по байтно заводской номер ДД
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	// восстанавливаем все обратно из оперативы
	Flash_ptr = (unsigned char *) (Addr_Segm5 + 3);                                 		// указатель на ячейку EEPROM (сегмент 5)
	prt = (unsigned char *) & tmpbuf;
	for (i_e=0; i_e<3; i_e++)
	{
		*Flash_ptr++ = *prt++;                                                              
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	 WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();
}


//==========================================================================================
// П/П записи в EEPROM калибровочных коэффициентов * Сегмент 1 * 0xFBFF-0xF800
//==========================================================================================

void EEPROM_write_call (void)
{
	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog
	_DINT();                                                                                // запрет всех прерываний
	volatile unsigned char i_e;                                                             //
	Flash_ptr = (unsigned char *) Addr_Segm1;                                               // Указываем на начало сегметна записи
	//	Код для полинома
	CALL_ptr  = (unsigned char *) & ca;                                                     //
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                       						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
//	FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных

	for (i_e=0; i_e<96; i_e++)
	{
		*Flash_ptr++ = *CALL_ptr++;                                                         // Записывыем по байтно весь массив калибровочных коэффициентов
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}
	//Конец код для полинома 

/* Код для кусочно линейной
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                       						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT; 

	*Flash_ptr = mass_pres_size;                                                            // размер массива давлений
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));

	*Flash_ptr = mass_res_size;                                                             // размер массива сопротивлений
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));

	CALL_ptr  = (unsigned char *) & mass_pres;
	for (i_e=0; i_e<24; i_e++)
	{
		*Flash_ptr++ = *CALL_ptr++;                                                         // Записывыем по байтно весь массив калибровочных коэффициентов
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	CALL_ptr  = (unsigned char *) & mass_res;
	for (i_e=0; i_e<16; i_e++)
	{
		*Flash_ptr++ = *CALL_ptr++;                                                         // Записывыем по байтно весь массив калибровочных коэффициентов
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}

	CALL_ptr  = (unsigned char *) & mass_value;
	for (i_e=0; i_e<96; i_e++)
	{
		*Flash_ptr++ = *CALL_ptr++;                                                         // Записывыем по байтно весь массив калибровочных коэффициентов
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
	}
*/// Код для кусочно линейной
	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();
}


//==========================================================================================
// П/П записи в EEPROM конфигурации датчика * сегмент 3 * 0xF3FF-0xF000
//==========================================================================================
void EEPROM_write_conf (void)
{
	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog
	_DINT();                                                                                // запрет всех прерываний
	Flash_ptr = (unsigned char *) Addr_EEPROM_conf;                                         // Указываем на начало сегметна записи
	if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                       						// If Info Seg is stil locked Clear LOCKSEG bit
	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
	//FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных

	// Запись данных в сегмент
	*Flash_ptr = N_integr;                                                                  // 32 измерения для интегрирования
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = tok_signal_inv;                                                            // выходной токовый сигнал 4-20 мА или 20-4 мА
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = edinica_izmer;                                                             // Единицы измерения
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = koren;                                                                     // Корнеизвлекающая зависимость отключена
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = DEV_ID;                                                                    // Адрес устройства
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = PREAMBULE;                                                                 // Количество преамбул
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = HART_disable;                                                              // Разрешение изменение конфигурации по HART
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = magnit_key_enable;                                                         // Разрешение работы магнитной кнопки
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	CALL_ptr = (unsigned char *) & diapazon_v;                                              // верхний предел 250 кПа
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & diapazon_n;                                              // нижний предел 0 кПа
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & damping_value;                                           // время демпфирования
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & delta_P;                                                 // значение для коррекции 0 давления
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & k_current;                                               // значение коэффициента усиления ЦАП
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & b_current;                                               // значение коэффициента смещения нуля ЦАП
	n = 2;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	Flash_ptr = (unsigned char *) Addr_EEPROM_conf + 30;
	*Flash_ptr = current_alarm;                                                             // Установка значения тока при появлении ошибки (Alarm)
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	Flash_ptr = (unsigned char *) Addr_EEPROM_conf + 125;
	*Flash_ptr = 0x00;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash
	Flash_ptr += 1;                                                                         //

	CALL_ptr = (unsigned char *) Addr_EEPROM_conf;

	CRC16 (CALL_ptr, 126);                                                                  // расчитываем значение CRC для блока данных с настройками датчика

	*Flash_ptr = crc_H;                                                                     //
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = crc_L;                                                                     //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	 WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();
}


//==========================================================================================
// П/П расчета CRC16 значение полинома 0xA001 
//==========================================================================================
void CRC16 (unsigned char *nach, unsigned char dlina)
{
	unsigned char j,flag_CRC;
	unsigned int temp_CRC;
	temp_CRC = 0xFFFF;
	while(dlina--)
	{
		temp_CRC ^= *nach++;
		for(j=0; j<8; j++)
		{
			flag_CRC = (unsigned char)(temp_CRC & 0x0001);
			temp_CRC = temp_CRC >> 1;
			if (flag_CRC)
			temp_CRC ^= 0xA001;
		}
	}
	crc_H= (unsigned char)(temp_CRC & 0x00FF);
	crc_L= (unsigned char)((temp_CRC & 0xFF00)>>8);
}

/*Закрыто для ЭнИ-12М
//==========================================================================================
// П/П Чтение настроек сохраненных предприятием изготовителем датчика из EEPROM
//==========================================================================================
void read_store_conf (void)
{
	tok_signal_inv =EEPROM_read (Addr_cur_EEPROM_conf + 1);
	edinica_izmer = EEPROM_read (Addr_cur_EEPROM_conf + 2);
	koren =         EEPROM_read (Addr_cur_EEPROM_conf + 3);
	DEV_ID =        EEPROM_read (Addr_cur_EEPROM_conf + 4);
	PREAMBULE =     EEPROM_read (Addr_cur_EEPROM_conf + 5);
	HART_disable =  EEPROM_read (Addr_cur_EEPROM_conf + 6);
	magnit_key_enable = EEPROM_read (Addr_cur_EEPROM_conf + 7);
	current_alarm = EEPROM_read (Addr_cur_EEPROM_conf + 30);
	//flagLang = EEPROM_read (Addr_cur_EEPROM_conf + 31);

	CALL_ptr = (unsigned char *) & diapazon_v;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 8);

	CALL_ptr = (unsigned char *) & diapazon_n;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 12);

	CALL_ptr = (unsigned char *) & damping_value;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 16);

	CALL_ptr = (unsigned char *) & delta_P;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 20);

	CALL_ptr = (unsigned char *) & k_current;
	READ_4BYTE (CALL_ptr, Addr_cur_EEPROM_conf + 24);

	CALL_ptr = (unsigned char *) & b_current;
	*CALL_ptr = EEPROM_read (Addr_cur_EEPROM_conf + 28);
	CALL_ptr += 1;
	*CALL_ptr = EEPROM_read (Addr_cur_EEPROM_conf + 29);
}   


//==========================================================================================
// П/П записи в EEPROM текущей конфигурации датчика * Сегмент 6 * 0xE7FF-0xE400
//==========================================================================================
void write_current_conf(void)
{
	WDTCTL = WDTPW + WDTHOLD;                                                               // выключаем watchdog
	_DINT();                                                                                // запрет всех прерываний
	Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf;                                     // Указываем на начало сегметна записи
    if(FCTL3 & LOCKSEG) FCTL3 = FWKEY | LOCKSEG;                    						// If Info Seg is stil lockedClear LOCKSEG bit

	FCTL1 = FWKEY + ERASE;                                                                  // Устанавливаем бит стирания сегмента
//	FCTL3 = FWKEY;                                                                          // убираем бит защиты от записи Lock bit
	*Flash_ptr = 0x00;                                                                      // Для стирания сегмента выполняем фиктивную запись
	while(FCTL3 & BUSY);                                                                    // Проверка готовности.
	FCTL1 = FWKEY + WRT;                                                                    // Устанавливаем режим записи блока данных

	// Запись данных в сегмент
	*Flash_ptr = N_integr;                                                                  // 32 измерения для интегрирования
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = tok_signal_inv;                                                            // выходной токовый сигнал 4-20 мА или 20-4 мА
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = edinica_izmer;                                                             // Единицы измерения
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = koren;                                                                     // Корнеизвлекающая зависимость отключена
	Flash_ptr += 1;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = DEV_ID;                                                                    // Адрес устройства
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = PREAMBULE;                                                                 // Количество преамбул
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = HART_disable;                                                              // Разрешение изменение конфигурации по HART
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = magnit_key_enable;                                                         // Разрешение работы магнитной кнопки
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	CALL_ptr = (unsigned char *) & diapazon_v;                                              // верхний предел 250 кПа
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & diapazon_n;                                              // нижний предел 0 кПа
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & damping_value;                                           // время демпфирования
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & delta_P;                                                 // значение для коррекции 0 давления
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & k_current;                                               // значение коэффициента усиления ЦАП
	n = 4;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	CALL_ptr = (unsigned char *) & b_current;                                               // значение коэффициента смещения нуля ЦАП
	n = 2;
	while (n)
	{
		*Flash_ptr = *CALL_ptr;
		Flash_ptr += 1; CALL_ptr += 1;
		while(!(FCTL3 & WAIT));                                                             // Ожидаем завершения записи во Flash
		n -= 1;
	}

	Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 30;
	*Flash_ptr = current_alarm;                                                             // Установка значения тока при появлении ошибки (Alarm)
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	// Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 31;
	// *Flash_ptr = flagLang;                                                             		// Установка текущего языка меню
	// while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	Flash_ptr = (unsigned char *) Addr_cur_EEPROM_conf + 125;
	*Flash_ptr = 0x00;
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash
	Flash_ptr += 1;                                                                         //

	CALL_ptr = (unsigned char *) Addr_cur_EEPROM_conf;

	CRC16 (CALL_ptr, 126);                                                                  // расчитываем значение CRC для блока данных с настройками датчика

	*Flash_ptr = crc_H;                                                                     //
	Flash_ptr += 1;                                                                         //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	*Flash_ptr = crc_L;                                                                     //
	while(!(FCTL3 & WAIT));                                                                 // Ожидаем завершения записи во Flash

	FCTL1 = FWKEY;                                                                          // сбрасываем бит записи WRT
	while(FCTL3 & BUSY);                                                                    // Если генератор flash закончил работу - устанавливаем бит защиты от записи
	FCTL3 = FWKEY | LOCKSEG;                    											// Set LOCKSEG bit
	 WDTCTL = WDT_START;                                                                     // включаем watchdog и обнуляем его таймер
	_EINT();
}                       
*/

//==========================================================================================
// П/П округления числа типа float 
//==========================================================================================
float okruglenie (float abc, unsigned char num)
{
	volatile float znc;
	volatile long y;
	abc = abc * 10 * num + 0.5;
	y = (long) abc;
	znc = (float) y / (10 * num);
	return znc;
}


//==========================================================================================
// П/П перевода в текущие ед. измерения
//==========================================================================================
float perevod_v_davlenie (float znach)
{
	switch (edinica_izmer)
	{
		case 0x0C: {znach = znach * mastb;                break;}                           // кПа
		case 0xED: {znach = znach * mastb * 0.00100000;   break;}                           // МПа
		case 0x05: {znach = znach * mastb * 7.50060000;   break;}                           // мм. рт. ст.
		case 0xEF: {znach = znach * mastb * 101.972000;   break;}                           // мм. вод. ст.
		case 0x0A: {znach = znach * mastb * 0.01019720;   break;}                           // кгс/см2
		case 0xB0: {znach = znach * mastb * 101.972000;   break;}                           // кгс/м2
		case 0x07: {znach = znach * mastb * 0.01000000;   break;}                           // бар
		case 0x0B: {znach = znach * mastb * 1000;         break;}                           // Па
		default: break;
	}
	return znach;
}


//==========================================================================================
// П/П перевода в текущие ед. измерения
//==========================================================================================
float perevod_v_uslovnie_edinici (float znach, char edinica)
{
	switch (edinica)
	{
		case 0x0C: {znach = znach / (mastb * 1.);           break;}                         // кПа
		case 0xED: {znach = znach / (mastb * 0.00100000);   break;}                         // МПа
		case 0x05: {znach = znach / (mastb * 7.50060000);   break;}                         // мм. рт. ст.
		case 0xEF: {znach = znach / (mastb * 101.972000);   break;}                         // мм. вод. ст.
		case 0x0A: {znach = znach / (mastb * 0.01019720);   break;}                         // кгс/см2
		case 0xB0: {znach = znach / (mastb * 101.972000);   break;}                         // кгс/м2
		case 0x07: {znach = znach / (mastb * 0.01000000);   break;}                         // бар
		case 0x0B: {znach = znach / (mastb * 1000);         break;}                         // Па
		default: break;
	}
	return znach;
}


//==========================================================================================
// П/П для проверки правильности коррекции нуля
//==========================================================================================
char proverka_delta_P (float delt_P)
{
	if (delt_P <= 0.05)                                                                     //
	{
		if (delt_P >= -0.05)                                                                //
		{
			return 0;                                                                       // возвращаем 0 если текущее смещение нуля не превышает +-5% от диапазона
		}
		else
		{
			return 2;                                                                       // возвращаем 2 если смещение меньше -5%
		}
	}
	else return 1;                                                                          // возвращаем 1 если смещение больше 5%
}

/*
//==========================================================================================
// П/П диагностики ошибок в датчике 
//==========================================================================================
void Search_problem (void)                                                                  
{
	if (type_error == EEPROM_ERR)                                                       // При ошибке EEPROM периодически выполняем П/П вывода аварийного тока
	{
		critical_error ();                                                              // Для того чтобы при смене настроек в меню тут же менялось значение аварийного тока (Hi / Lo)
	}
	else
	{
		if ((p <= diapazon_v * 1.1) && (p >= (diapazon_n - 0.1) * diapazon_v))                  // проверка на допустимый диаппазон давления +-10% от установленного диапазона, то есть если все хорошо с диапазоном, то
		{
			if(flag_error == 1)                                                                 // Если до сих пор выставлен флаг ошибки
			{
				if (type_error == PRES_OVER_HI || type_error == PRES_OVER_LO || type_error == SENS_ERR)                      // если была ошибка превышения давления или ошибка сенсора -
				{
					if (DEV_ID == 0)                                                            // и управления токовым выходом если датчик имеет нулевой адрес.
					{
						flag_i_out_auto = 1;
					}
					flag_error = 0;                                                             // Убираем флаг наличия ошибки и продолжаем работать дальше
					if(type_error != ALL_OK)                                                    // При этом если ошибка еще не была сброшена, то сбрасываем, если уже сброшена, то пропускаем
					{                                                                           // Пропуск сделан для того, чтобы не было артефактов свечения светодиода (при выполнении Set_error светодиод гаснет на мгновение)    
						type_error = ALL_OK;													// Сброс всех ошибок                                                  	
					}
				}
			}
		}
		else                                                                                    // Давление выходит за допустимые пределы
		{
			if ((type_error != EEPROM_ERR) && (type_error != PRES_OVER_HI) && (type_error != PRES_OVER_LO) && (type_error != SENS_ERR)) // Если не выставлена какая либо ошибка связанная с диапазоном и сенсором + ошибка EEPROM которая главнее
			{
				flag_error = 1;                                                                 // Выставляем флаг, что есть какая то ошибка с диапазоном и идем разбираться в какую сторону в плюс или минус она
				if (p >= diapazon_v * 1.1)    
				{
					if(type_error != PRES_OVER_HI)                                          	// Если код ошибки выше ВПИ еще не установлен, то устанавливаем
					{
						type_error = PRES_OVER_HI; 												// Код ошибки - Выше ВПИ                                          	
					}
				} 
				else                            
				{
					if(type_error != PRES_OVER_LO)                                          	// Если код ошибки ниже НПИ не установлен, то устанавливаем
					{
						type_error = PRES_OVER_LO; 												// Код ошибки - Ниже НПИ                                          	
					}	  
				}
			}
			if (type_error != EEPROM_ERR)							            // если нет ошибки ЕЕПРОМ проверяем работоспособность тензодатчика
			{
				if (b_ADC_mosta < 0x0020 || b_ADC_mosta > 0xFFFFE0)             // если код АЦП имеет крайние значения - неисправность сенсора
				{
					if(type_error != SENS_ERR)                                  // Если код ошибки - неисправность сенсора еще не выставлен, то выставляем
					{
						type_error = SENS_ERR;                                  // Код ошибки - неисправность сенсора
						critical_error ();
					}
				}
				else
				{
					if(type_error == SENS_ERR)                                  // Если код АЦП не выходит за пределы, то сброс ошибки обрыва сенсора
					{
						type_error = ALL_OK;                                              		
                        if (DEV_ID == 0) flag_i_out_auto = 1;                   // если датчик имеет нулевой адрес.// выход из режима фикс тока                                                        
					}
				}
			} 
		}
	}
}
*/

//==========================================================================================
// П/П вывода номера ошибки на индикатор при аварийных ситуациях и установка аварийного сигнала в токовой петле
//==========================================================================================
void critical_error (void)
{
	if (DEV_ID == 0)
	{
		flag_i_out_auto = 0;                                                                // Входим в режим фиксированного тока

		//if (current_alarm)  loop_current = CRIT_ERR_HIGH;         							// В зависимости от настроек указываем какой ток выводить при обнаружении критической ошибки
		//else                loop_current = CRIT_ERR_LOW;           							//

		loop_current = CRIT_ERR_LOW;

		OUT_current ();                                                                     // Устанавливаем аварийное значение тока.
	}
}

